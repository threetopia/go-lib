package utils

import (
	"os"
	"strings"
)

type ConfigUtilInterface interface {
	GetConfig(prefix string) map[string]string
}
// Load config with given prefix
func (cu Utils) LoadConfig(prefix string) (map[string]string) {
	envVar := make(map[string]string)
	for _, element := range os.Environ() {
		variable := strings.SplitN(element, "=", 2)
		hasPrefix := strings.HasPrefix(variable[0], prefix)
		if (hasPrefix) {
			variable[0] = strings.Replace(variable[0], prefix, "", -1)
			envVar[variable[0]] = variable[1]
		}
	}
	return envVar
}
