package utils

import (
	"fmt"
	"strconv"
)

func (cu Utils) SubInt(val int, start int, length int) int {
	s := fmt.Sprintf("%d", val)
	result, _ := strconv.Atoi(s[start:length])
	return result
}