package database

import (
	"github.com/go-redis/redis"
	"time"
	"encoding/json"
	"strings"
)

type mapStringRedisClient map[string]*redis.Client

type redisDatabase struct {
	redisConn mapStringRedisClient
	redisResult mapStringInterface
}

func (self *Database) RedisInit(alias string, address string, password string, database int) *Database {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password, // no password set
		DB:       database,  // use default DB
	})
	_, err := client.Ping().Result()
	if err != nil {
		self.err = make(mapStringError)
		self.err[alias] = err
	}
	self.redisConn = make(mapStringRedisClient)
	self.redisConn[alias] = client
	return self
}

func (self *Database) Set(alias string, key string, value string, expiration time.Duration) *Database {
	self.redisResult = make(mapStringInterface)
	err := self.redisConn[alias].Set(key, value, expiration)
	if err != nil {
		self.err = make(mapStringError)
		self.err[alias] = err.Err()
	}
	return self
}

func (self *Database) Get(alias string, key string) string {
	val, err := self.redisConn[alias].Get(key).Result()
	if err != nil {
		//panic(err)
	}
	return val
}

func (self *Database) Keys(alias string, key string) []string {
	val, err := self.redisConn[alias].Keys(key).Result()
	if err != nil {
		panic(err)
	}
	return val
}

func (self *Database) GetByKeys(alias string, key string) map[string]interface{} {
	results := make(map[string]interface{})
	keys := self.Keys(alias, key)
	for _, redisKey := range keys {
		if redisKey == "" {
			continue
		}
		data := self.Get(alias, redisKey)
		if data == "" {
			continue
		}
		var result map[string]interface{}
		d := json.NewDecoder(strings.NewReader(data))
		d.UseNumber()
		d.Decode(&result)
		results[redisKey] = result
	}
	return results
}

func (self *Database) Del(alias string, keys ...string) error {
	result := self.redisConn[alias].Del(keys...)
	return result.Err()
}