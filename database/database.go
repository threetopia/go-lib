package database

import (
	"time"
	"fmt"
	"strconv"
	"github.com/threetopia/go-lib/utils"
	"crypto/sha1"
)

type mapStringError map[string]error
type mapStringInterface map[string]interface{}
type Database struct {
	*mysqlDatabase
	*redisDatabase
	err mapStringError
}

func (Database) GeneratePrimaryID() int64 {
	cu := utils.Utils{}
	time := time.Now()
	dateTime := fmt.Sprintf("%d%02d%02d%02d%02d%02d%04d", time.Year(), time.Month(), time.Day(), time.Hour(), time.Minute(),time.Second(), cu.SubInt(time.Nanosecond(), 0, 4))
	runes := []rune(dateTime)
	primaryID, _ := strconv.ParseInt(string(runes[0:18]), 10,64)

	return primaryID
}

func (self Database) QueryExprToString(queryExpr interface{}) string {
	return fmt.Sprintf("%s", queryExpr)
}

func (self Database) QueryExprToSHA1String(queryExpr interface{}) string {
	query := []byte(self.QueryExprToString(queryExpr))
	return fmt.Sprintf("%x", sha1.Sum(query))
}

func (Database) checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
