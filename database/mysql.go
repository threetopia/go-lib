package database

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"strconv"
)

type mapStringSqlDB map[string]*sql.DB
type mapStringSqlRows map[string]*sql.Rows

type mysqlDatabase struct {
	mysqlConn mapStringSqlDB
	mysqlResult mapStringSqlRows
	mysqlExecResult mapStringInterface
}

func (self *Database) MySQLInit(alias string, connection string,) *Database {
	db, err := sql.Open("mysql", connection)
	self.checkErr(err)
	self.mysqlConn = make(mapStringSqlDB)
	self.mysqlConn[alias] = db
	return self
}

func (self *Database) MySQLQuery(alias string, query string, args ...interface{}) *Database {
	self.mysqlResult = make(mapStringSqlRows)
	self.err = make(mapStringError)
	self.mysqlResult[alias], self.err[alias] = self.mysqlConn[alias].Query(query, args...)
	return self
}

func (self *Database) MySQLPrepareExec(alias string, query string, args ...interface{}) *Database {
	prep, err := self.mysqlConn[alias].Prepare(query)
	self.checkErr(err)
	self.mysqlExecResult = make(mapStringInterface)
	exec, err := prep.Exec(args...)
	self.checkErr(err)
	self.mysqlExecResult[alias] = exec
	return self
}

func (self *Database) MySQLTotalData(alias string, table string) int {
	query := "SELECT count(*) as `count` FROM "  + table
	self.mysqlResult = make(mapStringSqlRows)
	self.err = make(mapStringError)
	self.mysqlResult[alias], self.err[alias] = self.mysqlConn[alias].Query(query)
	result := self.MySQLGetResults(alias)
	var count interface{}
	for _, v := range result {
		val := v.(map[string]interface{})
		count = val["count"]
	}
	cr := count.(string)
	rc, _ := strconv.Atoi(cr)
	return rc
}

func (self *Database) MySQLGetResults(alias string) []interface{} {
	rows := self.mysqlResult[alias]
	columns, _ := rows.Columns()
	count := len(columns)
	values := make([]interface{}, count)
	valuePointers := make([]interface{}, count)
	var dbResult []interface{}
	for rows.Next() {
		for i, _ := range columns {
			valuePointers[i] = &values[i]
		}
		rows.Scan(valuePointers...)
		result := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if (ok) {
				v = string(b)
			} else {
				v = val
			}
			result[col] = v
		}
		dbResult = append(dbResult, result)
	}
	if len(dbResult) == 0 {
		return nil
	}
	return dbResult
}

// Close the database connection
func (self *Database) MySQLClose(connection string) {
	defer self.mysqlConn[connection].Close()
}

func (self *Database) MySQLErr(connection string) error {
	return self.err[connection]
}
