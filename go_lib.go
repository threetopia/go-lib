package go_lib

import "github.com/threetopia/go-lib/database"

type GoLib struct {
	dblib *database.Database
}

func (self *GoLib) GetDBLib() *database.Database {
	return self.dblib
}